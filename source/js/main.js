$(function() {
    $(window).scrollTop(0);
    $('.text_btn').on('click', function(e) {
        e.preventDefault();
        $(this).parents('.text_wp').toggleClass('active');
    });
    $('.menu').on('click', function(e) {
        e.preventDefault();
        var $menu_list = $(this).parents('nav').siblings('.menu_list');
        $(this).toggleClass('active');
        $menu_list.toggleClass('show');
        if ($menu_list.hasClass('show')) {
            $menu_list.after("<div class='menu_ba'></div>");
            $('.menu_item').css('position','fixed');
            $('body').css('overflow','hidden');
        } else {
            $('.menu_ba').remove();
            $('.menu_item').css('position','initial');
            $('body').css('overflow','auto');
        }
    });


    // scroll to top 
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });
    $('.scrollup').click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });


    var current = 0;
    var _bx_ba = true;


    $('.bxslider').bxSlider({
        // controls: true,
        //        captions: true,
        auto: true,
        // autoControls: true,
        nextSelector: '#slider-next',
        prevSelector: '#slider-prev',
        nextText: '<div class="snext"></div>',
        prevText: '<div class="sprev"></div>',
        minSlides: 3,
        maxSlides: 3,
        startSlide: 0,
        slideMargin: 0,
        adaptiveHeight: true,
        onSliderLoad: function() {
            $bx_text();
        },
        onSlideAfter: function() {
            // increment current by 1 each time this method is called
            current += 1;
            $bx_text();
        }
    });

    function $bx_text() {
        var _bx_text = $('[aria-hidden=false] img').attr('title'),
            _bx_false = $('[aria-hidden=false]'),
            _bx_ture = $('[aria-hidden=ture]');
        if (_bx_ba) {
            $('.site').after(' <div class="bxslider_text"></div>');
            _bx_ba = false;
        }
        // console.log(_bx_text)
        if ($('.bxslider_text').length > 0) {
            $('.bxslider_text').html(_bx_text);
        }
        //                _bx_false.addClass('bx_ba').siblings(_bx_ture).removeClass('bx_ba');
    }


    function menu_item(title, text) {
        $('.menu_item').children('.' + title).on('click', function() {

            var $this = $(this).parent();
            var $siblings = $this.siblings();

            $this.siblings().removeClass('active_top');
            $this.find('.' + text).slideToggle("slow");
            $siblings.find('.' + text).slideUp('slow');

            if ($this.hasClass('active_top') == false) {
                $this.addClass('active_top')
            } else {
                $this.removeClass('active_top')

            }

        });
    }
    menu_item('title', 'text');

    // function changeRealThumb(slider, newIndex) {

    //     var $thumbS = $(".item-thumb");
    //     $thumbS.find('.active').removeClass("active");
    //     $thumbS.find('a[data-slide-index="' + newIndex + '"]').addClass("active");
    //     slider.goToSlide(parseInt(newIndex / 5));


    // }


});
